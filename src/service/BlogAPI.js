import request from 'utils/Api'

const getDataBlogs = () => {
  return request({
    url: `blogs`,
    method: 'GET'
  })
}

const getLimitBlogs = data => {
  return request({
    url: `blogs?page=${data}&limit=10`,
    method: 'GET'
  })
}

const searchBlogs = data => {
  return request({
    url: `blogs?search=${data}`,
    method: 'GET'
  })
}

const sortBlogs = data => {
  return request({
    url: `blogs?sortBy=${data.sortBy}&order=${data.order}`,
    method: 'GET'
  })
}

const getBlogDetail = id => {
  return request({
    url: `blogs/${id}`,
    method: 'GET'
  })
}

export { getDataBlogs, getLimitBlogs, searchBlogs, sortBlogs, getBlogDetail }
