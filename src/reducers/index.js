import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import Blog from './BlogReducer'

export default history =>
  combineReducers({
    router: connectRouter(history),
    blog: Blog
  })
