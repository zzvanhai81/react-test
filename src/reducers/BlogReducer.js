import {
  GET_ALL_BLOG_SUCCESS,
  GET_LIMIT_BLOG_SUCCESS,
  SEARCH_BLOG_SUCCESS,
  SORT_BLOG_SUCCESS,
  GET_BLOG_DETAIL_SUCCESS,
  SHOW_MESSAGE,
  ON_SHOW_LOADER
} from 'constants/ActionTypes'

const INIT_STATE = {
  alertMessage: '',
  showMessage: false,
  loader: false,
  totalItemsCount: 0,
  blogList: [],
  blogDetail: {}
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_ALL_BLOG_SUCCESS: {
      return {
        ...state,
        loader: false,
        totalItemsCount: action.payload.length
      }
    }
    case GET_LIMIT_BLOG_SUCCESS: {
      return {
        ...state,
        loader: false,
        blogList: action.payload
      }
    }
    case SEARCH_BLOG_SUCCESS: {
      return {
        ...state,
        loader: false,
        blogList: action.payload
      }
    }
    case SORT_BLOG_SUCCESS: {
      return {
        ...state,
        loader: false,
        blogList: action.payload
      }
    }
    case GET_BLOG_DETAIL_SUCCESS: {
      return {
        ...state,
        loader: false,
        blogDetail: action.payload
      }
    }
    case SHOW_MESSAGE: {
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false
      }
    }
    case ON_SHOW_LOADER: {
      return { ...state, loader: true }
    }
    default:
      return state
  }
}
