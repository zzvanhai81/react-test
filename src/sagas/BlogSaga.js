import { call, all, fork, put, takeEvery } from 'redux-saga/effects'
import { GET_ALL_BLOG, GET_BLOG_DETAIL, GET_LIMIT_BLOG, SEARCH_BLOG, SORT_BLOG } from 'constants/ActionTypes'
import {
  getAllBlogSuccess,
  getLimitBlogSuccess,
  searchBlogSuccess,
  sortBlogSuccess,
  getBlogDetailSuccess,
  showBlogMessage
} from 'actions/BlogAction'
import { getDataBlogs, getLimitBlogs, searchBlogs, sortBlogs, getBlogDetail } from 'service/BlogAPI'

function* getAllBlogRequest() {
  try {
    const res = yield call(getDataBlogs)
    if (res && res.status === 200 && res.data) {
      yield put(getAllBlogSuccess(res.data))
    } else {
      yield put(showBlogMessage(''))
    }
  } catch (error) {
    yield put(showBlogMessage(error))
  }
}

function* getLimitBlogRequest({ payload }) {
  try {
    const res = yield call(getLimitBlogs, payload)
    if (res && res.status === 200 && res.data) {
      yield put(getLimitBlogSuccess(res.data))
    } else {
      yield put(showBlogMessage(''))
    }
  } catch (error) {
    yield put(showBlogMessage(error))
  }
}

function* searchBlogRequest({ payload }) {
  try {
    const res = yield call(searchBlogs, payload)
    if (res && res.status === 200 && res.data) {
      yield put(searchBlogSuccess(res.data))
    } else {
      yield put(showBlogMessage(''))
    }
  } catch (error) {
    yield put(showBlogMessage(error))
  }
}

function* sortBlogRequest({ payload }) {
  try {
    const res = yield call(sortBlogs, payload)
    if (res && res.status === 200 && res.data) {
      yield put(sortBlogSuccess(res.data))
    } else {
      yield put(showBlogMessage(''))
    }
  } catch (error) {
    yield put(showBlogMessage(error))
  }
}

function* getBlogDetailRequest({ payload }) {
  try {
    const res = yield call(getBlogDetail, payload)
    if (res && res.status === 200 && res.data) {
      yield put(getBlogDetailSuccess(res.data))
    } else {
      yield put(showBlogMessage(''))
    }
  } catch (error) {
    yield put(showBlogMessage(error))
  }
}

export function* getAllBlogSaga() {
  yield takeEvery(GET_ALL_BLOG, getAllBlogRequest)
}

export function* getLimitBlogSaga() {
  yield takeEvery(GET_LIMIT_BLOG, getLimitBlogRequest)
}

export function* searchBlogSaga() {
  yield takeEvery(SEARCH_BLOG, searchBlogRequest)
}

export function* sortBlogSaga() {
  yield takeEvery(SORT_BLOG, sortBlogRequest)
}

export function* getBlogDetailSaga() {
  yield takeEvery(GET_BLOG_DETAIL, getBlogDetailRequest)
}

export default function* rootSaga() {
  yield all([
    fork(getAllBlogSaga),
    fork(getLimitBlogSaga),
    fork(searchBlogSaga),
    fork(sortBlogSaga),
    fork(getBlogDetailSaga)
  ])
}
