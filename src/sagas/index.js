import { all } from 'redux-saga/effects'
import Blog from './BlogSaga'

export default function* rootSaga(getState) {
  yield all([Blog()])
}
