import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { getBlogDetail } from 'actions'
import { useDispatch, useSelector } from 'react-redux'

const BlogDetail = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { blogDetail, loader } = useSelector(({ blog }) => blog)

  useEffect(() => {
    let id = new URLSearchParams(window.location.search).get('id')
    dispatch(getBlogDetail(id))
  }, [])

  return (
    <div class="container">
      <button type="button" class="btn btn-danger mt-4" onClick={() => navigate('/', { replace: true })}>
        Back
      </button>
      <div className="blog-detail-img">
        <img src={blogDetail.image} class="img-fluid" alt="Responsive image" />
      </div>
      <div className="media-body">
        <h5 className="mt-2 mb-1">{blogDetail.title}</h5>
        {blogDetail.content}
      </div>
    </div>
  )
}

export default BlogDetail
