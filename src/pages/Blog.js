import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getAllBlog, getLimitBlog, searchBlog, sortBlog, showBlogLoader } from 'actions'
import Articles from 'components/Articles'
import Pagination from 'components/Pagination'
import { useDebounce } from 'hooks/useDebound'

const Blog = () => {
  const dispatch = useDispatch()
  const { blogList, totalItemsCount, loader } = useSelector(({ blog }) => blog)
  const [activePage, setActivePage] = useState(1)
  const [sortData, setSortData] = useState({
    name: 'Sort by Date: ASC',
    sortBy: 'createdAt',
    order: 'asc'
  })
  const [searchKey, setSearchKey] = useState('')
  const debounceSearchQuery = useDebounce(searchKey, 1000)
  const [isOpenDropdown, setIsOpenDropdown] = useState(false)

  useEffect(() => {
    dispatch(getAllBlog())
  }, [])

  useEffect(() => {
    dispatch(showBlogLoader())
    dispatch(searchBlog(debounceSearchQuery))
  }, [debounceSearchQuery])

  useEffect(() => {
    dispatch(showBlogLoader())
    dispatch(sortBlog(sortData))
  }, [sortData])

  useEffect(() => {
    dispatch(showBlogLoader())
    dispatch(getLimitBlog(activePage))
  }, [activePage])

  const toggleOpen = () => {
    setIsOpenDropdown(!isOpenDropdown)
  }

  return (
    <div class="container">
      <div class="row mt-4">
        <div class="col-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search..."
              onChange={e => {
                e.persist()
                setSearchKey(e.target.value)
              }}
              value={searchKey}
            />
            <div className="input-group-prepend">
              <span className="input-group-text" id="basic-addon1">
                <img src={require('assets/images/icon-search.png')} />
              </span>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="col-4">
            <div className="dropdown" onClick={() => toggleOpen()}>
              <button
                className="btn btn-secondary dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
              >
                {sortData.name}
              </button>
              <div className={`dropdown-menu`} aria-labelledby="dropdownMenuButton">
                <div
                  className="dropdown-item"
                  onClick={() =>
                    setSortData({
                      name: 'Sort by Date: ASC',
                      sortBy: 'createdAt',
                      order: 'asc'
                    })
                  }
                >
                  Sort by Date: ASC
                </div>
                <div
                  className="dropdown-item"
                  onClick={() =>
                    setSortData({
                      name: 'Sort by Date: DESC',
                      sortBy: 'createdAt',
                      order: 'desc'
                    })
                  }
                >
                  Sort by Date: DESC
                </div>
                <div
                  className="dropdown-item"
                  onClick={() =>
                    setSortData({
                      name: 'Sort by Title: ASC',
                      sortBy: 'title',
                      order: 'asc'
                    })
                  }
                >
                  Sort by Title: ASC
                </div>
                <div
                  className="dropdown-item"
                  onClick={() =>
                    setSortData({
                      name: 'Sort by Title: DESC',
                      sortBy: 'title',
                      order: 'desc'
                    })
                  }
                >
                  Sort by Title: DESC
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {loader ? (
        <div className="loading">
          <div class="spinner-border text-danger" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      ) : (
        <Articles blogList={blogList} />
      )}
      <div className="d-flex justify-content-center mt-4">
        <Pagination totalItemsCount={totalItemsCount / 10} activePage={activePage} setActivePage={setActivePage} />
      </div>
    </div>
  )
}

export default Blog
