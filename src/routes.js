import { useRoutes, Navigate } from 'react-router-dom'
import Blog from 'pages/Blog'
import BlogDetail from 'pages/BlogDetail'

export default function Router() {
  return useRoutes([
    {
      path: '/',
      element: <Blog />
    },
    {
      path: '/blog-detail',
      element: <BlogDetail />
    }
  ])
}
