import { useNavigate } from 'react-router-dom'

const Articles = ({ blogList }) => {
  const navigate = useNavigate()

  return (
    <ul className="list-unstyled">
      {blogList.map((item, index) => {
        return (
          <li
            className="media mt-2 blog-item"
            key={index}
            onClick={() => navigate(`/blog-detail?id=${item.id}`, { replace: true })}
          >
            <div className="blog-img">
              <img src={item.image} className="mr-3" alt="..." />
            </div>
            <div className="media-body">
              <h5 className="mt-0 mb-1">{item.title}</h5>
              {item.content}
            </div>
          </li>
        )
      })}
    </ul>
  )
}

export default Articles
