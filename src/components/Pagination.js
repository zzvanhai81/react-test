const Pagination = ({ totalItemsCount, activePage, setActivePage }) => {
  const handlePrevPage = () => {
    if (activePage > 1) {
      setActivePage(activePage - 1)
    }
  }

  const handleNextPage = () => {
    if (activePage < totalItemsCount) {
      setActivePage(activePage + 1)
    }
  }

  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        <li className="page-item" onClick={() => handlePrevPage()}>
          <span className="page-link" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </span>
        </li>
        {[...Array(totalItemsCount)].map((item, index) => {
          return (
            <li
              className={`page-item ${activePage === index + 1 ? 'active' : ''}`}
              key={index}
              onClick={() => setActivePage(index + 1)}
            >
              <span className="page-link">{index + 1}</span>
            </li>
          )
        })}
        <li className="page-item" onClick={() => handleNextPage()}>
          <span className="page-link" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </span>
        </li>
      </ul>
    </nav>
  )
}

export default Pagination
