import {
  GET_ALL_BLOG,
  GET_ALL_BLOG_SUCCESS,
  GET_LIMIT_BLOG,
  GET_LIMIT_BLOG_SUCCESS,
  SEARCH_BLOG,
  SEARCH_BLOG_SUCCESS,
  SORT_BLOG,
  SORT_BLOG_SUCCESS,
  GET_BLOG_DETAIL,
  GET_BLOG_DETAIL_SUCCESS,
  ON_SHOW_LOADER,
  SHOW_MESSAGE
} from 'constants/ActionTypes'

export const getAllBlog = () => {
  return {
    type: GET_ALL_BLOG
  }
}

export const getAllBlogSuccess = data => {
  return {
    type: GET_ALL_BLOG_SUCCESS,
    payload: data
  }
}

export const getLimitBlog = data => {
  return {
    type: GET_LIMIT_BLOG,
    payload: data
  }
}

export const getLimitBlogSuccess = data => {
  return {
    type: GET_LIMIT_BLOG_SUCCESS,
    payload: data
  }
}

export const searchBlog = data => {
  return {
    type: SEARCH_BLOG,
    payload: data
  }
}

export const searchBlogSuccess = data => {
  return {
    type: SEARCH_BLOG_SUCCESS,
    payload: data
  }
}

export const sortBlog = data => {
  return {
    type: SORT_BLOG,
    payload: data
  }
}

export const sortBlogSuccess = data => {
  return {
    type: SORT_BLOG_SUCCESS,
    payload: data
  }
}

export const getBlogDetail = data => {
  return {
    type: GET_BLOG_DETAIL,
    payload: data
  }
}

export const getBlogDetailSuccess = data => {
  return {
    type: GET_BLOG_DETAIL_SUCCESS,
    payload: data
  }
}

export const showBlogMessage = message => {
  return {
    type: SHOW_MESSAGE,
    payload: message
  }
}

export const showBlogLoader = () => {
  return {
    type: ON_SHOW_LOADER
  }
}
